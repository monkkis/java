package com.example.myapplication2;

abstract class Account {
    protected int tilinumero;
    protected double saldo=0;
    protected double luottoraja=0;

    Account(int n) {
        tilinumero=n;
    }
    public void talleta(double rahaa) {
        saldo=saldo+rahaa;
    }

    public void nosta(double rahaa) {

    }

}

class Käyttötili extends Account {

    Käyttötili(int n){
        super(n);
    }

    @Override
    public void nosta(double rahaa) {
        if (saldo>=rahaa) {
            saldo=saldo-rahaa;
        }
        else {
            System.out.println("Saldo ei riitä nostoon. Saldosi on: "+saldo);
        }
    }

}

class Luottotili extends Account {

    Luottotili(int n){
        super(n);
        luottoraja=1000;
    }

    @Override
    public void nosta(double rahaa) {
        if (saldo+luottoraja>=rahaa) {
            saldo=saldo-rahaa;
        }
        else {
            System.out.println("Luotto ei riitä nostoon. Saldosi on: "+saldo+", luottorajasi on: "+luottoraja);
        }
    }

}

