
package com.example.myapplication2;

import java.util.ArrayList;

public class Bank {
    ArrayList<Account> tilit=new ArrayList<Account>();
    ArrayList<Customer> asiakkaat=new ArrayList<Customer>();
    ArrayList<Transaction> tapahtumat=new ArrayList<Transaction>();
    private int tilinumerointi=0;
    private int suoritus=0;

    public Bank() {
        //tilit.add(new Käyttötili(101));
        //tilit.add(new Luottotili(102));
    }

    public void addKäyttötili(String name) {
        Customer asiakas;
        tilit.add(new Käyttötili(tilinumerointi+1));
        if ((asiakas=etsiAsiakas(name))!=null){
            asiakas.addAccount(tilinumerointi+1);
        }
        else {
            asiakkaat.add(asiakas=new Customer(name));
            asiakas.addAccount(tilinumerointi+1);
        }

        tilinumerointi++;
    }

    public void addLuottotili() {
        tilit.add(new Luottotili(tilinumerointi+1));
        tilinumerointi++;
    }

    public void removeTili(int n) {

        for(int i = 0; i < tilit.size(); i++) {
            if (tilit.get(i).tilinumero==n)  {
                tilit.remove(i);
                break; //poistutaan for loopista
            }
        }
    }

    public Account etsiTili(int n) {
        Account koTili=null;

        for(int i = 0; i < tilit.size(); i++) {
            if (tilit.get(i).tilinumero==n)  {
                koTili=tilit.get(i);
            }

        }
        return koTili;
    }

    public Customer etsiAsiakas(String n) {
        Customer koAsiakas=null;

        for(int i = 0; i < asiakkaat.size(); i++) {
            if (n.equals(asiakkaat.get(i).nimi))  {
                koAsiakas=asiakkaat.get(i);
            }

        }
        return koAsiakas;
    }

    public Customer etsiAsiakastilinumerolla(int n) {
        Customer koAsiakas=null;

        for(int i = 0; i < asiakkaat.size(); i++) {
            //if (n.equals(asiakkaat.get(i).nimi))  {
            if (asiakkaat.get(i).tilit.contains(n))  {
                koAsiakas=asiakkaat.get(i);
            }

        }
        return koAsiakas;
    }

    public void talleta(Account tili, double rahaa) {
        tili.talleta(rahaa);
        tapahtumat.add(new Transaction(tili.tilinumero, rahaa,0));


    }

    public void nosta(Account tili, double rahaa) {
        tili.nosta(rahaa);
    }

    public void tulostaTili(Account tili){

        System.out.println(tili.getClass().getSimpleName()+", tilinumero: "+tili.tilinumero+", saldo: "+tili.saldo);

    }

    public void tulostaTilit(){
        for (Account tilsut : tilit) {
            System.out.println(tilsut.getClass().getSimpleName()+", tilinumero: "+tilsut.tilinumero+", saldo: "+tilsut.saldo);
        }
    }

    public String tulostaTilitAsText() {
        String ll="";
        Customer as;
        for (Account tilsut : tilit) {
            as=etsiAsiakastilinumerolla(tilsut.tilinumero);
            ll=ll+as.nimi+": Tilinumero: "+Integer.toString(tilsut.tilinumero)+" Tilillä rahaa: "+String.format("%.0f",tilsut.saldo)+"\n";
        }
        return ll;
    }
    public String tulostaTilitapahtumattAsText(int tilinro) {
        String ll="";
        Transaction tr;

        for (Transaction tapaht : tapahtumat) {
            if (tapaht.tili==tilinro){
                ll=ll+"Tapahtuma: Tili "+Integer.toString(tapaht.tili)+" Summa: "+String.format("%.0f",tapaht.summa)+"\n";
            }
            //tr=etsiAsiakastilinumerolla(tilsut.tilinumero);

        }
        return ll;
    }



}
