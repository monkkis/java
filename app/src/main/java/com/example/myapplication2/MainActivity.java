package com.example.myapplication2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;


public class MainActivity extends AppCompatActivity {


    TextView text;
    TextInputEditText textinput, summainput, summainput2, summainput3;
    Bank bank;
    String apu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.testFunction();
        text = (TextView) findViewById(R.id.textView);
        textinput=(TextInputEditText) findViewById(R.id.nimi_input);
        summainput=(TextInputEditText) findViewById(R.id.summa_input);
        summainput2=(TextInputEditText) findViewById(R.id.summa_input2);
        summainput3=(TextInputEditText) findViewById(R.id.summa_input3);
        bank=new Bank();
        //bank.addKäyttötili();
        //bank.tulostaTilit();
        ColorStateList oldColors =  text.getTextColors();


    }

    public void testFunction (View v) {

        //System.out.println("HELLO WORLD!");
        text.setText("Hello");
        //moi
        //bank.tulostaTilit();
        text.setText(textinput.getEditableText());
    }

    public void perusta_tili(View v){
        //apu=textinput.getEditableText().toString();
        //bank.addKäyttötili();
        if ((textinput.getEditableText().toString()).isEmpty()) {
            text.setText("Anna nimesi ensin.");
            //text.setHighlightColor(Color.RED);
            //text.setHighlightColor(Color.);

            return;
        }
        bank.addKäyttötili(textinput.getEditableText().toString());
        //Bank bank=new Bank();
        apu=bank.tulostaTilitAsText();
        text.setText(apu);
    }

    public void teetilitapahtuma(View v){
        //apu=textinput.getEditableText().toString();
        //bank.addKäyttötili();

        if (((summainput.getEditableText().toString()).isEmpty()) || ((summainput2.getEditableText().toString()).isEmpty())) {
            text.setText("Anna ensin summa ja tilinumero");
            return;
        }

        if ((summainput3.getEditableText().toString()).isEmpty()){
            bank.talleta(bank.etsiTili(Integer.parseInt(summainput2.getEditableText().toString())), Double.parseDouble(summainput.getEditableText().toString()));
        }
        else {
            bank.talleta(bank.etsiTili(Integer.parseInt(summainput2.getEditableText().toString())), -1 * Double.parseDouble(summainput.getEditableText().toString()));
            bank.talleta(bank.etsiTili(Integer.parseInt(summainput3.getEditableText().toString())), Double.parseDouble(summainput.getEditableText().toString()));
            //text.setText("moi");
        }
        apu=bank.tulostaTilitAsText();
        text.setText(apu);
        summainput.setText("");
        summainput2.setText("");
        summainput3.setText("");
    }

    public void tulostatilintapahtumat(View v){
        if ((summainput2.getEditableText().toString()).isEmpty()){
            text.setText("Et antanut tilinumeroa. Anna tilinumero.");
        }
        else {
            apu = bank.tulostaTilitapahtumattAsText(Integer.parseInt(summainput2.getEditableText().toString()));
            text.setText(apu);
        }
    }

}
